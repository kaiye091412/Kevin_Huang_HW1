package Q3;

public class Q3 {
	public static String reverse(String input) {
		int lastIndex = input.length()-1;   //do this so that for loop doesn't check length each time
		for(int i=lastIndex; i>-1;i--) {
			input += input.charAt(i);
		}
		return input.substring(lastIndex+1);
	}
}
