package Q4;

public class Q4 {
	public static int nFactorial(int n) {
		if(n<1) {	// N<1 doesn't make sense, return 0
			System.out.println("N must be greater than 0");
			return 0;
		}
		
		int factorial=1;
		for(int i=1; i<=n; i++) {
			factorial *=i;
		}
		return factorial;
	}
}
