package Q1;

public class Q1 {
	
	private static final void swap(int[] input, int a, int b) { //provide an abstraction
		int temp = input[a];
		input[a] = input[b];
		input[b] = temp;
	}
	
	public static void bubbleSort(int[] input) {  //O(n^2)
		boolean isSorted = false;
		while(!isSorted) {
			isSorted = true;
			for(int i=0;i<input.length-1;i++) {
				if(input[i]>input[i+1]) {
					swap(input, i, i+1);
					isSorted = false;
				}
			}
		}
	}
}
