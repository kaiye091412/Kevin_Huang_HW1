package Q19;

import java.util.ArrayList;

public class Q19 {
	public static void arrayListDemo() {
		ArrayList<Integer> list= new ArrayList<Integer>();
		for(int i=1;i<11;i++) {
			list.add(i);
		}
		for(Integer i : list) {
			System.out.println(i);
		}
		//
		System.out.println("sum of even:");
		int sumEven = 0;
		for(Integer i:list) {
			if(i%2==0) sumEven+=i;
			System.out.println(sumEven);
		}
		
		//
		System.out.println("sum of odd:");
		int sumOdd = 0;
		for(Integer i:list) {
			if(i%2==1) sumOdd+=i;
			System.out.println(sumOdd);
		}
		
		ArrayList newList =  new ArrayList<Integer>(); //get non-prime numbers in this newList
		for(Integer i : list) {
			for(int n=2; n<11;n++) {
				if(i!=1 && i!=2 && i>n && i%n==0) {  // 1 and 2 are prime numbers
					newList.add(i);
				}
			}
		}
		list = newList;  //assign newList to list, cannot remove whlie iterating through.
		
	}
	
}
