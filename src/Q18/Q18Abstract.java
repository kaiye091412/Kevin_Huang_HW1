package Q18;

public abstract class Q18Abstract {
	abstract boolean checkUppercase(String s);
	abstract String toUpper(String s);
	abstract int toIntPlusTen(String s);
}

