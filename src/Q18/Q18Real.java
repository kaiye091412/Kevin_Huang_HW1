package Q18;

public class Q18Real extends Q18Abstract {

	@Override
	boolean checkUppercase(String s) {
		for(int i=0;i<s.length();i++) {
			if(Character.isUpperCase(s.charAt(i))) {
				return true;
			}
		}
		return false;
	}

	@Override
	String toUpper(String s) {
		StringBuilder answer=new StringBuilder();
		for(int i=0;i<s.length();i++) {
			answer.append(Character.toUpperCase(s.charAt(i)));
		}
		return answer.toString();
	}

	@Override
	int toIntPlusTen(String s) {
		int temp = Integer.parseInt(s);
		return temp+10;
	}
	
	
}
