package Q8;

import java.util.ArrayList;
import java.util.Arrays;

public class Q8 {
	public static ArrayList<String> palindromes(){
		ArrayList<String> input = new ArrayList<String>(Arrays.asList("karan", "madam", 
				"tom", "civic", "radar", "sexes", "jimmy", "kayak", "john", "refer", 
				"billy","did"));
		
		ArrayList<String> palindromes = new ArrayList<String>();
		
		for(String s:input) {
			boolean isPalin =true; //default is true, detect those that are not palindrome
			for(int i=0;i<s.length()/2;++i) {
				if(s.charAt(i)!=s.charAt(s.length()-1-i)) {  //is any two pair of chars is not the same, break
					isPalin = false;
					break;
				}
			}
			if(isPalin) palindromes.add(s);
		}
		return palindromes;
	}
}
