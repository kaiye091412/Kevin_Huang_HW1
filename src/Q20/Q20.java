package Q20;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Q20 {
	public static void parseTXT() throws IOException
	{
		Path currentRelativePath = Paths.get("");
		String s = currentRelativePath.toAbsolutePath().toString();
		System.out.println("Current relative path is: " + s);
		BufferedReader br;
		String strLine;
		File f = new File("./Data.txt");
		FileReader fr = new FileReader(f);
		br = new BufferedReader(fr);
		while ((strLine = br.readLine()) != null)   {
			  String[] sArray = strLine.split(":");
			  System.out.println("Name: "+sArray[0]+" "+sArray[1]);
			  System.out.println("Age: "+sArray[2]+"years");
			  System.out.println("State: "+sArray[3]+"State");
			}
		br.close();
	}
}
