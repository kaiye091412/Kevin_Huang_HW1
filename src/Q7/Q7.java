package Q7;

import java.util.ArrayList;
import java.util.Comparator;

class Employee{
	String name;
	String department;
	int age;
}

public class Q7 {
	public static void sortEmployee(ArrayList<Employee> input) {
		input.sort(new Comparator<Employee>() {

			@Override
			public int compare(Employee o1, Employee o2) {  // sort based on their age
				if(o1.age==o2.age) return 0;	//could to custom method
				return o1.age<o2.age? -1:1;
			}
			
		});
	}
}
