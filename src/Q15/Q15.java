package Q15;

interface oprators{
	int addition(int a, int b);
	int subtraction(int a, int b);
	int multiplication(int a, int b);
	int division(int a, int b);
}

public class Q15 implements oprators{

	@Override
	public int addition(int a, int b) {
		return a+b;
	}

	@Override
	public int subtraction(int a, int b) {
		return a-b;
	}

	@Override
	public int multiplication(int a, int b) {
		return a*b;
	}

	@Override
	public int division(int a, int b) {
		return a/b;
	}


}
