package TestDriver;

import java.io.IOException;

import Q1.Q1;
import Q13.Q13;
import Q14.Q14;
import Q15.Q15;
import Q2.Q2;
import Q20.Q20;
import Q3.Q3;
import Q4.Q4;
import Q5.Q5;
import Q6.Q6;

public class TestDriver {

	public static void main(String[] args) {
		int[] test1 = new int[] {1,0,5,6,3,2,3,7,9,8,4};
		Q1.bubbleSort(test1);
		for(int i : test1) {
			System.out.print(i);
		}
		////////////////////////////////////
		System.out.println();
		Q2.fibonacci();
		
		////////////////////////////////////
		System.out.println("");
		System.out.println(Q3.reverse("KevinHuang"));
		////////////////////////////////////
		System.out.println("");
		System.out.println(Q4.nFactorial(4));
		////////////////////////////////////
		System.out.println("");
		System.out.println(Q5.subString("abcdefg", 3));
		////////////////////////////////////
		System.out.println("");
		System.out.println(Q6.isEven(4));
		
		Q13.printLoop();
		Q14.demoSwitch(2);
		Q15 q15 = new Q15();
		System.out.println(q15.addition(1, 2));
		try {
			Q20.parseTXT();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
