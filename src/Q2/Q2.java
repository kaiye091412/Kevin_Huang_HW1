package Q2;

public class Q2 {
	
	public static void fibonacci() {
		int count = 25;  // number of Fibonacci numbers wanted
		int[] lastNumber= new int[]{0,1};
		int toMod = 0; //which index of lastNumber to Modify
		System.out.print(0 +" ");
		System.out.print(1 +" ");
		for(int i=2; i<count;i++) {
			int newNumber = lastNumber[0]+lastNumber[1];
			System.out.print(newNumber +" ");
			lastNumber[toMod] = newNumber;
			toMod = (toMod+1)%2;   //0=>1   1=>0
		}
	}
}
