package Q9;

import java.util.ArrayList;

public class Q9 {
	public static void printPrime() {
		ArrayList<Integer> input= new ArrayList<Integer>();
		for(int i=1; i<101;i++) {
			input.add(i);
		}
		
		for(Integer n : input) {
			if(isPrime(n)) {
				System.out.println(n);
			}
		}
	}
	
	private static boolean isPrime(int n) {
		if (n%2==0) return false;  //making less iteration for the below for loop
	    for(int i=3;i*i<=n;i+=2) {  //only need to check 0 to squared root of n
	        if(n%i==0)
	            return false;
	    }
	    return true;
	}
}
