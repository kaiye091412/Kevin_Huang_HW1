package Q12;

public class Q12 {
	public static void printEven() {
		int[] array=new int[100];
		for(int i=1;i<101;i++) { //store 0-100 to the array
			array[i-1] =i;
		}
		
		for(int i:array) {
			if(i%2==0) {
				System.out.println(i);
			}
		}
	}
}
