package Q13;

public class Q13 {
	public static void printLoop() {
		int beginInt =0;
		for(int i=0;i<4;i++) {
			if(i==1 ||i==2) beginInt=1;
			if(i==0 ||i==3) beginInt=0;
			for(int k=0;k<i+1;k++) {
				System.out.print(beginInt+" ");
				beginInt = (beginInt+1)%2;
			}
			System.out.println("");
		}
	}
}
